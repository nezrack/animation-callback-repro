import { animate, AnimationEvent, style, transition, trigger } from '@angular/animations';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-sub',
  templateUrl: './sub.component.html',
  styleUrls: ['./sub.component.scss'],
  animations: [
    trigger('fadeIn', [
      transition(':enter', [
        style({opacity: 0}),
        animate('0.5s', style({opacity: 1}))
      ])
    ]),
    trigger('fadeOut', [
      transition(':leave', [
        style({opacity: 1}),
        animate('0.5s', style({opacity: 0}))
      ])
    ])
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SubComponent {

  @Input() state: number;

  fadeIn: boolean;

  constructor(private cd: ChangeDetectorRef) {
  }

  fadedOut(event: AnimationEvent): void {
    if (event.fromState === null && event.toState === 'void') {
      this.fadeIn = true;
      this.cd.markForCheck();
    }
  }

}
